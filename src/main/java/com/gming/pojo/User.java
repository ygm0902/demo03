package com.gming.pojo;

/**
 * @ClassName: User
 * @Description:
 * @Author: ygm
 * @Date: 21:32 2020/4/6
 * @Version 2.1
 **/
public class User {
    private Integer id;
    private String username;
    private String address;

    public User() {
    }

    public User(Integer id, String username) {
        this.id = id;
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
